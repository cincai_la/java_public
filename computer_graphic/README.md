# Computer graphic Uni Project

This is the final assignment of the university subject. The project includes create a world with elevation/altitudes. And the first person view able to feel the change in altitude when walking on the terrain. By pressing space bar, the avatar will show (a TEAPOT).

Trees are drawn using geometry and L-system. L-system can draw cool tree.

Also, a dinosaur has been imported. However, it has not been colored. 
Note that JSON file is used to create world. More trees, terrains, L-System tree, can be easily added as stocks.

## Video of the sample world
[Video](https://youtu.be/86JzPNMVljo)

## Image of the sample world
![Image of the OpenGL World](https://i9.ytimg.com/vi/86JzPNMVljo/mq1.jpg?sqp=CKz-hIMG&rs=AOn4CLCheO9v2VaE6xRLs8et1yUyexPWAw)


