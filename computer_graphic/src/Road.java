import java.util.ArrayList;
import java.util.List;

//import javax.media.opengl.GL2;
import com.jogamp.opengl.GL2;

/**
 * COMMENT: Comment Road 
 *
 * @author 
 */
public class Road {
	private int numPoints = 30;
    private List<Double> myPoints;
    private double myWidth;
    private List<Double> myVertices;
    
    /** 
     * Create a new road starting at the specified point
     */
    public Road(double width, double x0, double y0) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        myVertices = new ArrayList<Double>();
        myPoints.add(x0);
        myPoints.add(y0);
    }

    /**
     * Create a new road with the specified spine 
     *
     * @param width
     * @param spine
     */
    public Road(double width, double[] spine) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        myVertices = new ArrayList<Double>();
        for (int i = 0; i < spine.length; i++) {
            myPoints.add(spine[i]);
        }
    }
    
    /**
     * The width of the road.
     * 
     * @return
     */
    public double width() {
        return myWidth;
    }

    /**
	 * @return the list of points
	 */
	public List<Double> getMyPoints() {
		return myPoints;
	}

	/**
     * Add a new segment of road, beginning at the last point added and ending at (x3, y3).
     * (x1, y1) and (x2, y2) are interpolated as bezier control points.
     * 
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     */
    public void addSegment(double x1, double y1, double x2, double y2, double x3, double y3) {
        myPoints.add(x1);
        myPoints.add(y1);
        myPoints.add(x2);
        myPoints.add(y2);
        myPoints.add(x3);
        myPoints.add(y3);        
    }
    
    /**
     * Get the number of segments in the curve
     * 
     * @return
     */
    public int size() {
        return myPoints.size() / 6;
    }

    /**
     * Get the specified control point.
     * 
     * @param i
     * @return
     */
    public double[] controlPoint(int i) {
        double[] p = new double[2];
        p[0] = myPoints.get(i*2);
        p[1] = myPoints.get(i*2+1);
        return p;
    }
    
    /**
     * Get a point on the spine. The parameter t may vary from 0 to size().
     * Points on the kth segment take have parameters in the range (k, k+1).
     * 
     * @param t
     * @return
     */
    public double[] point(double t) {
        int i = (int)Math.floor(t);
        t = t - i;
        
        i *= 6;
        
        double x0 = myPoints.get(i++);
        double y0 = myPoints.get(i++);
        double x1 = myPoints.get(i++);
        double y1 = myPoints.get(i++);
        double x2 = myPoints.get(i++);
        double y2 = myPoints.get(i++);
        double x3 = myPoints.get(i++);
        double y3 = myPoints.get(i++);
        
        double[] p = new double[2];

        p[0] = b(0, t) * x0 + b(1, t) * x1 + b(2, t) * x2 + b(3, t) * x3;
        p[1] = b(0, t) * y0 + b(1, t) * y1 + b(2, t) * y2 + b(3, t) * y3;        
        
        return p;
    }
    
    /**
     * Calculate the Bezier coefficients
     * 
     * @param i
     * @param t
     * @return
     */
    private double b(int i, double t) {
        
        switch(i) {
        
        case 0:
            return (1-t) * (1-t) * (1-t);

        case 1:
            return 3 * (1-t) * (1-t) * t;
            
        case 2:
            return 3 * (1-t) * t * t;

        case 3:
            return t * t * t;
        }
        
        // this should never happen
        throw new IllegalArgumentException("" + i);
    }

    public void draw(GL2 gl, MyTexture texture) {
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, texture.getTextureId());
    	double tIncrement = 1.0/numPoints;
    	
    	//lower road strip
    	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
    		for (int i = 0; i < numPoints*(myPoints.size()/6)-1; i++) {
    			double t = i*tIncrement;
    			double t1 = (i+1)*tIncrement;
    			
    			double x = point(t)[0];
    			double z = point(t)[1];
    			gl.glNormal3d(0, 1, 0);
    			gl.glTexCoord2d(x, z);
    			gl.glVertex3d(x, 0, z);
    			
    			double[] T = {point(t1)[0]-x, 0, point(t1)[1]-z};
    			T = normalise(T);
    			double[] y = {0, 1, 0};
    			double[] n = cross(T, y);    			
    			gl.glTexCoord2d(x+n[0]*myWidth/2, z+n[2]*myWidth/2);
    			gl.glVertex3d(x+n[0]*myWidth/2, 0, z+n[2]*myWidth/2);
    			
    			//keep track of spine vertices
    			if(myVertices.size() < numPoints*2 - 2) {
    				myVertices.add(x);
    				myVertices.add(z);
    			}
    		}
    		//connect to final point
    		int j = myPoints.size()/2 - 1;
    		int k = myVertices.size() - 3;
    		double[] u = {controlPoint(j)[0], 0, controlPoint(j)[1]};
    		gl.glNormal3d(0, 1, 0);
    		gl.glTexCoord2d(u[0], u[2]);
    		gl.glVertex3d(u[0], 0, u[2]);
    		
    		if(myVertices.size() < numPoints*2) {
				myVertices.add(u[0]);
				myVertices.add(u[2]);
			}
    		
    		double[] u1 = {u[0]-myVertices.get(k-1), 0, u[2]-myVertices.get(k)};
    		u1 = normalise(u1);
    		double[] v = {0, 1, 0};
    		double[] norm = cross(u1, v);    		
    		gl.glTexCoord2d(u[0]+norm[0]*myWidth/2, u[2]+norm[2]*myWidth/2);
    		gl.glVertex3d(u[0]+norm[0]*myWidth/2, 0, u[2]+norm[2]*myWidth/2);
    	gl.glEnd();
    	
    	//upper road strip
    	gl.glBegin(GL2.GL_TRIANGLE_STRIP);
    		for (int i = 0; i < myVertices.size()-3; i+=2) {
    			double x = myVertices.get(i);
    			double z = myVertices.get(i+1);
    			gl.glNormal3d(0, 1, 0);
    			gl.glTexCoord2d(x, z);
    			gl.glVertex3d(x, 0, z);
    			
    			double[] T = {myVertices.get(i+2)-x, 0, myVertices.get(i+3)-z};
    			T = normalise(T);
    			double[] y = {0, -1, 0};
    			double[] n = cross(T, y);    			
    			gl.glTexCoord2d(x+n[0]*myWidth/2, z+n[2]*myWidth/2);
    			gl.glVertex3d(x+n[0]*myWidth/2, 0, z+n[2]*myWidth/2);
    		}
    		//connect to final point
    		gl.glNormal3d(0, 1, 0);
    		gl.glTexCoord2d(u[0], u[2]);
    		gl.glVertex3d(u[0], 0, u[2]);
    		
    		v[1] = -1;
    		norm = cross(u1, v);    		
    		gl.glTexCoord2d(u[0]+norm[0]*myWidth/2, u[2]+norm[2]*myWidth/2);
    		gl.glVertex3d(u[0]+norm[0]*myWidth/2, 0, u[2]+norm[2]*myWidth/2);
    	gl.glEnd();
    }
    
    /**
     * 
     * @param n 
     * @return magnitude of a vector
     */
    double getMagnitude(double[] n){
    	double mag = n[0]*n[0] + n[1]*n[1] + n[2]*n[2];
    	mag = Math.sqrt(mag);
    	return mag;
    }
    
    /**
     * 
     * @param n
     * @return normalised vector
     */
    double [] normalise(double [] n){
    	double  mag = getMagnitude(n);
    	double norm[] = {n[0]/mag,n[1]/mag,n[2]/mag};
    	return norm;
    }
    
    /**
     * 
     * @param u
     * @param v
     * @return the cross product of two vectors
     */
    public double [] cross(double u [], double v[]){
    	double crossProduct[] = new double[3];
    	crossProduct[0] = u[1]*v[2] - u[2]*v[1];
    	crossProduct[1] = u[2]*v[0] - u[0]*v[2];
    	crossProduct[2] = u[0]*v[1] - u[1]*v[0];
    	
    	return crossProduct;
    }
}
