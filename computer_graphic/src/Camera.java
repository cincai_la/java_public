import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

//import javax.media.opengl.GL2;
//import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

public class Camera implements KeyListener{
	private float[] myBackground;
	private double myScale;
	private double myRotation;
	private double[] myTranslation;

    private boolean showAvatar = true;
    private boolean nightMode = false;

    public Camera(float[] background, double scale, double rotation, double[] translation) {
        myBackground = background;
        myScale = scale;
        myRotation = rotation;
        myTranslation = translation;
    }
    
    public float[] getBackground() {
        return myBackground;
    }

    public void setBackground(float[] background) {
        myBackground = background;
    }
	
	public void setView(GL2 gl, Terrain myTerrain, Avatar avatar) {
		//added code for camera height
		Double height = 0.00;
		avatar.SetShowAvatar(this.showAvatar); 
		if (avatar.ShowAvatar()){
			double[] tt = new double[2];
			tt[1] = myScale*avatar.getPosition()[2]*Math.cos(myRotation/180* Math.PI);
			tt[0] = myScale*avatar.getPosition()[2]*Math.sin(myRotation/180* Math.PI);
			height = myTerrain.altitude(myTranslation[0] + tt[0], myTranslation[2] + tt[1]);
			Double camHeight = myTerrain.altitude(myTranslation[0], myTranslation[2]);
			if (!camHeight.isNaN()){
				if (camHeight > height)height = camHeight;
			}
		}
		else{
			height = myTerrain.altitude(myTranslation[0], myTranslation[2]);
		}

		if (!height.isNaN()){
			myTranslation[1] = height;
		}

    	//Set background colour
		if (nightMode) {
			float[] black = {0, 0, 0, 1.0f};
			gl.glClearColor(black[0], black[1], black[2], black[3]);
			myTerrain.setNightMode(true);
		} else {
			gl.glClearColor(myBackground[0], myBackground[1], myBackground[2], myBackground[3]);
			myTerrain.setNightMode(false);
		}
    	
    	gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        //Set the view matrix to account for the camera's position         
        gl.glScaled(1/myScale, 1/myScale, 1/myScale);
        gl.glRotated(-myRotation, 0, 1, 0);  
        gl.glTranslated(-myTranslation[0], -(myTranslation[1]+0.5), -myTranslation[2]);
    }

    public void reshape(GL2 gl, int x, int y, int width, int height) {
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        
        GLU glu = new GLU();
        glu.gluPerspective(80.0, (float)width/(float)height, 0.01, 40.0);
    }
    
    @Override
	public void keyPressed(KeyEvent e) {
		 switch (e.getKeyCode()) {
		 case KeyEvent.VK_UP:
				 myTranslation[0]-= 0.1 *Math.sin(myRotation/180* Math.PI);
				 myTranslation[2]-= 0.1 *Math.cos(myRotation/180* Math.PI);
			 break;
		 case KeyEvent.VK_DOWN:
				 myTranslation[0]+= 0.1 *Math.sin(myRotation/180* Math.PI);
				 myTranslation[2]+= 0.1 *Math.cos(myRotation/180* Math.PI);
			 break;
		 case KeyEvent.VK_RIGHT:
			 this.myRotation -= 3;
			 if (this.myRotation < 0) this.myRotation = this.myRotation + 360;
			 break;
		 case KeyEvent.VK_LEFT:
			 this.myRotation += 3;
			 if (this.myRotation > 360) this.myRotation = this.myRotation - 360;
			 break;
		 case KeyEvent.VK_SPACE:
			 if (this.showAvatar){
				 this.showAvatar = false;
			 }
			 else {
				 this.showAvatar = true;
			 }
			 break;
		 case KeyEvent.VK_N:
			 if (nightMode) {
				 nightMode = false;
			 } else {
				 nightMode = true;
			 }
			 break;
		 default:
			 break;
		 }
    }

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
}
