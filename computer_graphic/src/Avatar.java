//import javax.media.opengl.GL2;
import com.jogamp.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

public class Avatar {

    private double[] myPos;
    private String textureFileName1 = "src/teapot.jpg";
    private String textureExt1 = "jpg";
    private MyTexture myTextures[];
    private boolean showAvatar = false;
    
    public Avatar(double z) {
        myPos = new double[3];
        myPos[0] = 0;
        myPos[1] = 0;
        myPos[2] = z;
    }
    
    public double[] getPosition() {
        return myPos;
    }
    
    public void drawAvatar(GL2 gl){
    	myTextures = new MyTexture[1];
    	myTextures[0]= new MyTexture(gl, textureFileName1, textureExt1, true);
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[0].getTextureId());
    	gl.glTranslated(this.myPos[0], -0.5, this.myPos[2]);
		gl.glRotated(90, 0, 1, 0);
		gl.glScaled(0.3, 0.3, 0.3);
		GLUT glut = new GLUT();
        glut.glutSolidTeapot(1);
        
    }
   	
    
    public void normalize(double v[])  
    {  
        double d = Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);  
        if (d != 0.0) 
        {  
           v[0]/=d; 
           v[1]/=d;  
           v[2]/=d;  
        }  
    }
    
    
    public boolean ShowAvatar(){
    	return showAvatar;
    }
    
    public void SetShowAvatar(boolean setShowAvatar){
    	this.showAvatar = setShowAvatar;
    }
    

}
