import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

//import javax.media.opengl.GL2;
import com.jogamp.opengl.GL2;


/**
 * COMMENT: Comment HeightMap 
 *
 * @author malcolmr
 */
public class Terrain {
    private Dimension mySize;
    private double[][] myAltitude;
    private List<Tree> myTrees;
    private List<Road> myRoads;
    private List<Other> myOthers;
    private float[] mySunlight;
    private boolean nightMode;

    private List<LSystem> myLSystems;

    private String textureFileName1 = "src/snow.jpg";
    private String textureExt1 = "jpg";
    private String textureFileName2 = "src/road2.jpg";
    private String textureExt2 = "jpg";
    private String textureFileName3 = "src/trunk2.jpg";
    private String textureExt3 = "jpg";
    private String textureFileName4 = "src/branch.jpg";
    private String textureExt4 = "jpg";
    private String textureFileName5 = "src/ice.jpg";
    private String textureExt5 = "jpg";
    private MyTexture myTextures[];
    
    public void init(GL2 gl) {
    	myTextures = new MyTexture[8];
    	myTextures[0]= new MyTexture(gl, textureFileName1, textureExt1, true);
    	myTextures[1] = new MyTexture(gl, textureFileName2, textureExt2, true);
    	myTextures[2] = new MyTexture(gl, textureFileName3, textureExt3, true);
    	myTextures[3] = new MyTexture(gl, textureFileName4, textureExt4, true);
    	myTextures[4] = new MyTexture(gl, textureFileName5, textureExt5, true);
    }

    /**
     * Create a new terrain
     *
     * @param width The number of vertices in the x-direction
     * @param depth The number of vertices in the z-direction
     */
    public Terrain(int width, int depth) {
        mySize = new Dimension(width, depth);
        myAltitude = new double[width][depth];
        myTrees = new ArrayList<Tree>();
        myRoads = new ArrayList<Road>();
        myOthers = new ArrayList<Other>();
        mySunlight = new float[3];
        myLSystems = new ArrayList<LSystem>();
    }
    
    public Terrain(Dimension size) {
        this(size.width, size.height);
    }

    public Dimension size() {
        return mySize;
    }

    public List<Tree> trees() {
        return myTrees;
    }

    public List<Road> roads() {
        return myRoads;
    }
    
    public List<Other> others() {
    	return myOthers;
    }

    public float[] getSunlight() {
        return mySunlight;
    }
    
    public List<LSystem> LSystems() {
        return this.myLSystems;
    }

    /**
     * Set the sunlight direction. 
     * 
     * Note: the sun should be treated as a directional light, without a position
     * 
     * @param dx
     * @param dy
     * @param dz
     */
    public void setSunlightDir(float dx, float dy, float dz) {
        mySunlight[0] = dx;
        mySunlight[1] = dy;
        mySunlight[2] = dz;
    }
    
    /**
     * Set night mode
     * 
     * @param nightMode
     */
    public void setNightMode(boolean nightMode) {
    	this.nightMode = nightMode;
    }
    
    /**
     * Resize the terrain, copying any old altitudes. 
     * 
     * @param width
     * @param height
     */
    public void setSize(int width, int height) {
        mySize = new Dimension(width, height);
        double[][] oldAlt = myAltitude;
        myAltitude = new double[width][height];
        
        for (int i = 0; i < width && i < oldAlt.length; i++) {
            for (int j = 0; j < height && j < oldAlt[i].length; j++) {
                myAltitude[i][j] = oldAlt[i][j];
            }
        }
    }

    /**
     * Get the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public double getGridAltitude(int x, int z) {
    	if (x < 0 || x >= myAltitude.length || z < 0 || z >= myAltitude.length) return 0;
    	
        return myAltitude[x][z];
    }

    /**
     * Set the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public void setGridAltitude(int x, int z, double h) {
    	if (x < 0 || x >= myAltitude.length || z < 0 || z >= myAltitude.length) return;
    	
        myAltitude[x][z] = h;
    }

    /**
     * Get the altitude at an arbitrary point. 
     * Non-integer points should be interpolated from neighbouring grid points
     * 
     * TO BE COMPLETED
     * 
     * @param x
     * @param z
     * @return
     */
    public double altitude(double x, double z) {    	
    	// Andrew: bilinear interpolation is used
//        double altitude = 0;
    	double altitude;
    	int x_f = (int)Math.floor(x);
    	int x_c = (int)Math.ceil(x);
    	int z_f = (int)Math.floor(z);
    	int z_c = (int)Math.ceil(z);
    	if (z_c == z_f){
    		z_f = z_f-1;
    	}
    	if (x_c == x_f){
    		x_f = x_f - 1;
    	}
    	
    	// interpolation z
    	if (x_f < 0 || z_f < 0){
    		altitude = 0;
    		return altitude;
    	}
    	double q1 = this.getGridAltitude(x_f, z_f);
    	double q2 = this.getGridAltitude(x_f, z_c);
    	double al_l = (z - z_f)/ (z_c - z_f)*q2 + (z_c - z) / (z_c - z_f) * q1;
    	
    	double q1_2 = this.getGridAltitude(x_c, z_f);
    	double q2_2 = this.getGridAltitude(x_c, z_c);
    	double al_r = (z - z_f)/ (z_c - z_f)*q2_2 + (z_c - z) / (z_c - z_f) * q1_2;
    	
    	// interpolation x
    	// find (x, altitude, z) with (x_f, al_l, z)  and (x_c, al_r, z)
    	altitude = (x - x_f) / (x_c-x_f)* al_r + (x_c - x) / (x_c-x_f) * al_l;
        return altitude;
    }

    /**
     * Add a tree at the specified (x,z) point. 
     * The tree's y coordinate is calculated from the altitude of the terrain at that point.
     * 
     * @param x
     * @param z
     */
    public void addTree(double x, double z) {
        double y = altitude(x, z);
        Tree tree = new Tree(x, y, z);
        myTrees.add(tree);
    }

    public void addLSystem(double x, double z, int level){
    	double y = altitude(x, z);
    	LSystem lsystem = new LSystem(x, y, z, level);
    	this.myLSystems.add(lsystem);
    }

    /**
     * Add a road. 
     * 
     * @param x
     * @param z
     */
    public void addRoad(double width, double[] spine) {
        Road road = new Road(width, spine);
        myRoads.add(road);        
    }
    
    public void addOther(double x, double z) {
        double y = altitude(x, z);
        Other other = new Other(x, y, z);
        myOthers.add(other);
    }
    
    /**
     * Computes the face normal for each triangle
     * 
     * @param u vector between point 2 and point 1
     * @param v vector between point 3 and point 1
     * @return the cross product of u and v
     */
    public double[] computeNormals(double[] u, double[] v) {
    	double[] normal = new double[3];
    	
    	normal[0] = (u[1]*v[2]) - (u[2]*v[1]);
    	normal[1] = (u[2]*v[0]) - (u[0]*v[2]);
    	normal[2] = (u[0]*v[1]) - (u[1]*v[0]);
    	
    	return normal;
    }
    
    /**
     * Draws the terrain
     * 
     * @param gl
     */

    public void draw(GL2 gl) {    	
    	//Set up lighting
    	gl.glEnable(GL2.GL_LIGHT0);
    	gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, getSunlight(), 0);
    	
    	float[] ambient = {0.5f, 0.5f, 0.5f, 1.0f};    	
        float[] diffuse = {0.8f, 0.75f, 0.85f, 1.0f};
        float[] specular = {0.0f, 0.0f, 0.0f, 1.0f};
        
        //turn of the nights if in night mode
    	if (nightMode) {
    		float[] dark = {0, 0, 0, 1.0f};
    		ambient = dark;
    		diffuse = dark;
    		specular = dark;
    	}
    	
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffuse, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 0);
        
        //Set up materials
        float matAmbAndDif[] = {1.0f, 1.0f, 1.0f, 1.0f};
    	gl.glMaterialfv(GL2.GL_FRONT_AND_BACK, GL2.GL_AMBIENT_AND_DIFFUSE, matAmbAndDif, 0);
        
        //Draw terrain
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[0].getTextureId());
    	
    	gl.glBegin(GL2.GL_TRIANGLES);
    		for (int z = 0; z < mySize.height-1; z++) {
    			for (int x = 0; x < mySize.width-1; x++) {
    				double[] u = {0, getGridAltitude(x, z+1)-getGridAltitude(x, z), 1};
    				double[] v = {1, getGridAltitude(x+1, z)-getGridAltitude(x,z), 0};
    				double[] n = computeNormals(u, v);
    				
    				gl.glNormal3d(n[0], n[1], n[2]);
    				gl.glTexCoord2d(x, z);
    				gl.glVertex3d(x, getGridAltitude(x, z), z);
    				gl.glTexCoord2d(x, z+1);
    				gl.glVertex3d(x, getGridAltitude(x, z+1), z+1);
    				gl.glTexCoord2d(x+1, z);
    				gl.glVertex3d(x+1, getGridAltitude(x+1, z), z);
    			}
    		}
    		
    		for (int z = 1; z < mySize.height; z++) {
    			for (int x = 1; x < mySize.width; x++) {
    				double[] u = {0, getGridAltitude(x, z-1)-getGridAltitude(x, z), -1};
    				double[] v = {-1, getGridAltitude(x-1, z)-getGridAltitude(x,z), 0};
    				double[] n = computeNormals(u, v);
    				
    				gl.glNormal3d(n[0], n[1], n[2]);
    				gl.glTexCoord2d(x, z);
    				gl.glVertex3d(x, getGridAltitude(x, z), z);
    				gl.glTexCoord2d(x, z-1);
    				gl.glVertex3d(x, getGridAltitude(x, z-1), z-1);
    				gl.glTexCoord2d(x-1, z);
    				gl.glVertex3d(x-1, getGridAltitude(x-1, z), z);
    			}
    		}
    	gl.glEnd();
    	
    	//Draw trees
    	for (Tree temp: myTrees){
    		gl.glPushMatrix();
    		double[] myPos = temp.getPosition();
    		gl.glTranslated(myPos[0], myPos[1], myPos[2]);
    		gl.glScaled(0.8, 0.8, 0.8);
    		temp.drawTree(gl, this.myTextures);
    		gl.glPopMatrix();
    	}
    	
    	//Draw roads
    	for (Road road : myRoads) {
    		gl.glPushMatrix();
    		gl.glTranslated(0, altitude(road.getMyPoints().get(0), road.getMyPoints().get(1)) + 0.01, 0);
    		road.draw(gl, myTextures[1]);
    		gl.glPopMatrix();
    	}
    	
		// F : draw cylinder mesh and translate (0,0.25,0)
		// Y : rotate (25, 1, 0, 0)
		// Z : rotate (-25, 1, 0, 0)
		// S : scale(0.5, 0.5, 0.5)
		// U : push
		// P : pop
		// X : does nothing
    	
    	for (LSystem temp : this.myLSystems) {
    		gl.glPushMatrix();
    		double[] myPos = temp.getPosition();
    		gl.glTranslated(myPos[0], myPos[1], myPos[2]);
    		try {
				temp.leveGeneration(gl, myTextures);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		gl.glPopMatrix();
    	}
    	
    	//Draw other objects
    	for (Other other : myOthers) {
    		gl.glPushMatrix();
    		double[] myPos = other.getPosition();
    		gl.glTranslated(myPos[0], myPos[1], myPos[2]);
//    		gl.glScaled(0.5, 0.5, 0.5);
    		other.draw(gl, myTextures[4]);
    		gl.glPopMatrix();
    	}
    }

}
