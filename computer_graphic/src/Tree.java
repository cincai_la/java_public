import com.jogamp.opengl.GL2;

/**
 * COMMENT: Comment Tree 
 *
 * @author 
 */
public class Tree {

    private double[] myPos;
    
    public Tree(double x, double y, double z) {
        myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y;
        myPos[2] = z;
    }
    
    public double[] getPosition() {
        return myPos;
    }
    
    public void drawTree(GL2 gl, MyTexture[] myTextures){
    	double z1 = 0;
    	double z2 = 2;
    	double radius = 0.15;
    	
    	double slices = 30;
    	
    	
    	gl.glBegin(GL2.GL_TRIANGLE_FAN);{
    		gl.glNormal3d(0,-1,0);
    		gl.glVertex3d(0,z1,0);
    		double angleStep = 2*Math.PI/slices;
    		for (int i = 0; i <= slices ; i++){//slices; i++) {
    			double a0 = i * angleStep;
    			double a1 = ((i+1) % slices) * angleStep;
                
                //Calculate vertices for the quad
                double x0 = radius* Math.cos(a0);
                double y0 = radius* Math.sin(a0);

               gl.glVertex3d(x0,z1,y0);
            }
                
                
   	}gl.glEnd();
   	
   	gl.glBegin(GL2.GL_TRIANGLE_FAN);{
       	
  		 gl.glNormal3d(0,1,0);
  		 gl.glVertex3d(0,z2,0);
  		 double angleStep = 2*Math.PI/slices;
           for (int i = 0; i <= slices ; i++){//slices; i++) {
               double a0 = i * angleStep;
              
               
               //Calculate vertices for the quad
               double x0 = radius* Math.cos(a0);
               double y0 = radius* Math.sin(a0);

              gl.glVertex3d(x0,z2,y0);
           }
               
               
  	}gl.glEnd();
  	
  	
  	gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[2].getTextureId());
   	gl.glBegin(GL2.GL_QUAD_STRIP);
       {
    	   
           double angleStep = 2*Math.PI/slices;
           
           
           for (int i = 0; i <= slices ; i++){//slices; i++) {
               double a0 = i * angleStep;
               double a1 = ((i+1) % slices) * angleStep;
               
               double x0 = radius* Math.cos(a0);
               double y0 = radius* Math.sin(a0);

              
               double sCoord = 1.0/slices * i; //Or * 2 to repeat label
	        	
	        	
               gl.glNormal3d(x0, 0, y0);
               gl.glTexCoord2d(sCoord,0); 
               gl.glVertex3d(x0, z1, y0);
               gl.glTexCoord2d(sCoord,1);
               gl.glVertex3d(x0, z2, y0);  
                
//              
           }

       }
       gl.glEnd();
       
       gl.glTranslated(0.0, z2, 0);
       gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[3].getTextureId());

       double deltaT;
       int maxStacks = 10;
       int maxSlices = 20;
   	
   	deltaT = 0.5/maxStacks;
   	int ang;  
   	int delang = 360/maxSlices;
   	double x1,x2,y1,y2;
   	radius = 1;
   	for (int i = 0; i < maxStacks; i++) 
   	{ 
   		double t = -0.25 + i*deltaT;
   		
   		gl.glBegin(GL2.GL_TRIANGLE_STRIP); 
   		for(int j = 0; j <= maxSlices; j++)  
   		{  
   			ang = j*delang;
   			x1=radius * r(t)*Math.cos((double)ang*2.0*Math.PI/360.0); 
   			x2=radius * r(t+deltaT)*Math.cos((double)ang*2.0*Math.PI/360.0); 
   			y1 = radius * getY(t);

   			z1=radius * r(t)*Math.sin((double)ang*2.0*Math.PI/360.0);  
   			z2= radius * r(t+deltaT)*Math.sin((double)ang*2.0*Math.PI/360.0);  
   			y2 = radius * getY(t+deltaT);

   			double normal[] = {x1,y1,z1};


   			normalize(normal);    

   			gl.glNormal3dv(normal,0);  
   			double tCoord = 1.0/maxStacks * i; //Or * 2 to repeat label
   			double sCoord = 1.0/maxSlices * j;
   			gl.glTexCoord2d(sCoord,tCoord);
   			gl.glVertex3d(x1,y1,z1);
   			normal[0] = x2;
   			normal[1] = y2;
   			normal[2] = z2;

   			normalize(normal);    
   			gl.glNormal3dv(normal,0); 
   			tCoord = 1.0/maxStacks * (i+1); //Or * 2 to repeat label
   			gl.glTexCoord2d(sCoord,tCoord);
   			gl.glVertex3d(x2,y2,z2); 

   		}; 
   		gl.glEnd();}  
    }
   	
    private double r(double t){
    	double x  = Math.cos(2 * Math.PI * t);
        return x;
    }
    
    private double getY(double t){
    	
    	double y  = Math.sin(2 * Math.PI * t);
        return y;
    }
    
    public void normalize(double v[])  
    {  
        double d = Math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);  
        if (d != 0.0) 
        {  
           v[0]/=d; 
           v[1]/=d;  
           v[2]/=d;  
        }  
    } 
    

}
