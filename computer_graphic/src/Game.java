import java.io.File;

import java.io.FileNotFoundException;

//import javax.media.opengl.GL;
//import javax.media.opengl.GL2;
//import javax.media.opengl.GLAutoDrawable;
//import javax.media.opengl.GLCapabilities;
//import javax.media.opengl.GLEventListener;
//import javax.media.opengl.GLProfile;
//import javax.media.opengl.awt.GLJPanel;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;



import javax.swing.JFrame;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * COMMENT: Comment Game
 *
 * @author malcolmr
 */
public class Game extends JFrame implements GLEventListener{
    private Terrain myTerrain;
    private Camera camera;
    private Avatar avatar;

    public Game(Terrain terrain) {
    	super("Assignment 2");
    	this.avatar = new Avatar(-1);
        myTerrain = terrain;
    }
    
    /** 
     * Run the game.
     *
     */
    public void run() {
    	  GLProfile glp = GLProfile.getDefault();
          GLCapabilities caps = new GLCapabilities(glp);
          GLJPanel panel = new GLJPanel();
          panel.addGLEventListener(this);

          float[] background = {0.9f, 0.95f, 1.0f, 1.0f};
          double scale = 0.4;
          double rotation = 220;
          double[] translation = {0, 0, 0};
          this.camera = new Camera(background, scale, rotation, translation);

          panel.addKeyListener(camera);

          // Add an animator to call 'display' at 60fps        
          FPSAnimator animator = new FPSAnimator(60);
          animator.add(panel);
          animator.start();

          getContentPane().add(panel);
          setSize(800, 600);        
          setVisible(true);
          setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

	/**
     * Load a level file and display it.
     * 
     * @param args - The first argument is a level file in JSON format
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Terrain terrain = LevelIO.load(new File(args[0]));
        
        Game game = new Game(terrain);
        game.run();
    }

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		
		// added clear buffer by andrew
		//Set background colour
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		// Andrew added code for camera height
		//double height = -0.8;
		
		//set the view matrix based on the camera position
		if (this.avatar.ShowAvatar()){
			gl.glMatrixMode(GL2.GL_MODELVIEW);
			gl.glLoadIdentity();
			gl.glPushMatrix();
			this.avatar.drawAvatar(gl);		
			gl.glPopMatrix();    
		}
		
		camera.setView(gl, myTerrain, avatar);

        myTerrain.draw(gl);
        
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
    	gl.glEnable(GL2.GL_DEPTH_TEST);
		
		//Set up lighting
    	gl.glEnable(GL2.GL_LIGHTING);
        
		gl.glEnable(GL2.GL_NORMALIZE);
    	
    	gl.glEnable(GL2.GL_TEXTURE_2D);
    	myTerrain.init(gl);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
        GL2 gl = drawable.getGL().getGL2();

        camera.reshape(gl, x, y, width, height);
	}
}
