#version 130

varying vec2 texCoord;

void main (void) {	
	gl_Position = ftransform();
	texCoord = vec2(gl_MultiTexCoord0);
}

