#version 130

in vec2 texCoord;
uniform sampler2D texUnit;

void main (void) {	
	gl_FragColor = texture2D(texUnit, texCoord);
}