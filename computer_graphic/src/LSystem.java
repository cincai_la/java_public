import java.util.ArrayList;
import java.util.HashMap;

//import javax.media.opengl.GL2;
import com.jogamp.opengl.GL2;

//import com.sun.javafx.collections.MappingChange.Map;


/**
 * 
 */

/**
 * @author Chin Chai
 *
 */
public class LSystem {
	String grammar = "";
	HashMap<String, String> mapGrammar = new HashMap<String, String>();
	String command = "";
	int mylevel = 1;
	ArrayList<String> keys = new ArrayList<String>();
	private double[] myPos;
	
	public LSystem (double x, double y, double z, int level){
		grammar = "->X F Z U U X P Y X P Y F U Y F X P Z X ->F FF ->Y Y ->U U ->Z Z ->S S ->P P";
        myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y;
        myPos[2] = z;
        mylevel = level;
		this.grammarProcedure();
	}
	
    public double[] getPosition() {
        return myPos;
    }
	
	private void grammarProcedure(){
		
		grammar = grammar.replaceAll("\\s+", "");
//		System.out.println(grammar);
		
		while (grammar.indexOf(">") != -1 ){
			String text = grammar.substring(grammar.indexOf(">") + 1, grammar.indexOf(">")+2);
			// the LHS of grammar
			grammar = grammar.substring(grammar.indexOf(">")+2);
			
			// the RHS of grammar
			if (grammar.indexOf(">") != -1){
				String temp = grammar.substring(0, grammar.indexOf(">")-1);
				mapGrammar.put(text, temp);
			}
			else {
				mapGrammar.put(text, grammar);
			}
			
		}
		
		for (String name: mapGrammar.keySet()){
			String key =name.toString();
            keys.add(key);
            String value = mapGrammar.get(name).toString();  
            System.out.println(key + " " + value); 
		}
	}
	
	public void leveGeneration(GL2 gl, MyTexture[] myTextures) throws Exception{
		// F : draw cylinder mesh and translate (0,0.5,0)
		// Y : rotate (25, 1, 0, 0)
		// Z : rotate (-25, 1, 0, 0)
		// S : scale(0.5, 0.5, 0.5)
		// U : push
		// P : pop
		// X : does nothing
		
		// input Levels can not be smaller than 1
		if (this.mylevel <1){
			throw new Exception("LSystem generation level input incorrect");
		}
		String genLSystem = mapGrammar.get("X");
		String newGenLSystem = "";
		for (int i =0; i < this.mylevel; i++){
			if (i == 0) continue;
			else {
				for (int j = 0; j <  genLSystem.length(); j++){
					if (genLSystem.charAt(j) == "X".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("X"));
					}
					else if (genLSystem.charAt(j) == "F".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("F"));
					}
					
					// Y : rotate (25, 1, 0, 0)
					// Z : rotate (-25, 1, 0, 0)
					// S : scale(0.5, 0.5, 0.5)
					// U : push
					// P : pop
					
					else if (genLSystem.charAt(j) == "Y".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("Y")); 
					}
					else if (genLSystem.charAt(j) == "Z".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("Z"));
					}
					else if (genLSystem.charAt(j) == "S".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("S")); 
					}
					else if (genLSystem.charAt(j) == "U".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("U")); 
					}
					else if (genLSystem.charAt(j) == "P".toCharArray()[0]){
						newGenLSystem = newGenLSystem.concat(mapGrammar.get("P")); 
					}
					else {
//						throw new Exception("LSystem Syntax error");
					}
				}
			}
			genLSystem = newGenLSystem;
			newGenLSystem = "";
		}
		// debug LSystem
//		System.out.println("Final  = " + genLSystem);
		
		
		// F : draw cylinder mesh and translate (0,0.5,0)
		// Y : rotate (25, 1, 0, 0)
		// Z : rotate (-25, 1, 0, 0)
		// S : scale(0.5, 0.5, 0.5)
		// U : push
		// P : pop
		// X : does nothing

		for (int j = 0; j <  genLSystem.length(); j++){
			if (genLSystem.charAt(j) == "F".toCharArray()[0]){
				this.drawCylinder(gl, myTextures);
				gl.glTranslated(0, 0.25, 0);
			}
			else if (genLSystem.charAt(j) == "Y".toCharArray()[0]){
				gl.glRotated(25, 1, 0, 0);
			}
			else if (genLSystem.charAt(j) == "Z".toCharArray()[0]){
				gl.glRotated(-25, 1, 0, 0);
			}
			else if (genLSystem.charAt(j) == "S".toCharArray()[0]){
				gl.glScaled(0.7, 0.7, 0.7);
			}
			else if (genLSystem.charAt(j) == "U".toCharArray()[0]){
				gl.glPushMatrix();
			}
			else if (genLSystem.charAt(j) == "P".toCharArray()[0]){
				gl.glPopMatrix();
			}
			else if (genLSystem.charAt(j) == "X".toCharArray()[0]){
			}
			else {
				throw new Exception("LSystem Syntax error");
			}
		}
		
	}
	
	private void drawCylinder(GL2 gl, MyTexture[] myTextures){
    	double z1 = 0;
    	double z2 = 0.25;
    	double radius = 0.05;
    	double slices = 30;
    	
    	gl.glBegin(GL2.GL_TRIANGLE_FAN);
		gl.glNormal3d(0,-1,0);
		gl.glVertex3d(0,z1,0);
		double angleStep = 2*Math.PI/slices;
		for (int i = 0; i <= slices ; i++){
			double a0 = i * angleStep;
            
            //Calculate vertices for the quad
            double x0 = radius* Math.cos(a0);
            double y0 = radius* Math.sin(a0);
            gl.glVertex3d(x0,z1,y0);
        }
    	gl.glEnd();
    	
    	gl.glBegin(GL2.GL_TRIANGLE_FAN);
    	gl.glNormal3d(0,1,0);
    	gl.glVertex3d(0,z2,0);
    	angleStep = 2*Math.PI/slices;
    	for (int i = 0; i <= slices ; i++){
    		double a0 = i * angleStep;
    		
    		//Calculate vertices for the quad
    		double x0 = radius* Math.cos(a0);
    		double y0 = radius* Math.sin(a0);
    		gl.glVertex3d(x0,z2,y0);
    		}
    	gl.glEnd();
    	
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[2].getTextureId());
    	gl.glBegin(GL2.GL_QUAD_STRIP);
       {
    	   angleStep = 2*Math.PI/slices;
           for (int i = 0; i <= slices ; i++){//slices; i++) {
               double a0 = i * angleStep;
               
               double x0 = radius* Math.cos(a0);
               double y0 = radius* Math.sin(a0);

              
               double sCoord = 1.0/slices * i; //Or * 2 to repeat label
	        	
	        	
               gl.glNormal3d(x0, 0, y0);
               gl.glTexCoord2d(sCoord,0); 
               gl.glVertex3d(x0, z1, y0);
               gl.glTexCoord2d(sCoord,1);
               gl.glVertex3d(x0, z2, y0);  
                
           }

       }
       gl.glEnd();
		
		
	}

}
