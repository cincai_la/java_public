public class Point {
    public double x;
    public double y;
    public double z;
    
    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;        
    }

    public Point(double[] p) {
        this.x = p[0];
        this.y = p[1];
        this.z = p[2];        
    }
}
