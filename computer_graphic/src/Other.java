import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//import javax.media.opengl.GL2;
import com.jogamp.opengl.GL2;

/**
 * Dinosaur model downloaded from: http://tf3dm.com/3d-model/an-4125.html
 * 
 * @author Michele Goh
 *
 */
public class Other {
	private double[] myPos;
	private List<Point> vertices = new ArrayList<Point>();
	private List<Point> textures = new ArrayList<Point>();
	private List<Point> normals = new ArrayList<Point>();
	private List<Point> faces = new ArrayList<Point>();
	
	private DoubleBuffer vertexData;
	private DoubleBuffer textureData;
	private DoubleBuffer normalData;
	private int bufferIds[] = new int[1];
	
	private int shaderProgram;
	private static final String VERTEX_SHADER = "src/VertexShader.glsl";
	private static final String FRAGMENT_SHADER = "src/FragmentShader.glsl";
	private int texUnitLoc;
	
	public Other(double x, double y, double z) {
		myPos = new double[3];
        myPos[0] = x;
        myPos[1] = y;
        myPos[2] = z;
	}
	
    public double[] getPosition() {
        return myPos;
    }
	
    /**
     * Draws the object
     * @param gl
     */
	public void draw(GL2 gl, MyTexture texture) {
		if (vertices.size() == 0 || textures.size() == 0 || normals.size() == 0) init(gl);
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, texture.getTextureId());
		
		//Enable client states
		gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
		gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
		
		//Set shader and bind buffer
		gl.glUseProgram(shaderProgram);
		gl.glUniform1i(texUnitLoc, 0);
		int bufferSize = faces.size()*3*Double.BYTES;
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, bufferIds[0]);
		
		//Specify array locations
		gl.glNormalPointer(GL2.GL_DOUBLE, 0, 0);
		gl.glTexCoordPointer(2, GL2.GL_DOUBLE, 0, bufferSize);
		gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, bufferSize*2);
		
		//Draw the object
		gl.glDrawArrays(GL2.GL_TRIANGLES, 0, faces.size()*3-1);
		
		//Disable client states and unbind buffer
		gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
		gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
		gl.glUseProgram(0);
	}
	
	/**
	 * Loads vertices, texture coordinates, vertex normals, and faces from a text file into a VBO
	 */
	public void init(GL2 gl) {
		//Scan the file
		Scanner scanLine = null;
		try {
			scanLine = new Scanner(new FileReader("src/dinosaur.txt"));
			while (scanLine.hasNextLine()) {
				String firstToken = scanLine.next();
				String line = scanLine.nextLine();
				
				Scanner sc = new Scanner(line);
				while (sc.hasNext()) {
					if(firstToken.equals("v")) {
						//vertex
						double x = sc.nextDouble();
						double y = sc.nextDouble();
						double z = sc.nextDouble();
						Point p = new Point(x, y, z);
						vertices.add(p);
					} else if(firstToken.equals("vt")) {
						//texture
						double x = sc.nextDouble();
						double y = sc.nextDouble();
						Point p = new Point(x, y, 0);
						textures.add(p);
					} else if(firstToken.equals("vn")) {
						//normal
						double x = sc.nextDouble();
						double y = sc.nextDouble();
						double z = sc.nextDouble();
						Point p = new Point(x, y, z);
						normals.add(p);
					} else if(firstToken.equals("f")) {
						//face
						String input = sc.next();
						Scanner sf = new Scanner(input);
						sf.useDelimiter("/");
						double v = sf.nextDouble();
						double t = sf.nextDouble();
						double n = sf.nextDouble();
						Point p = new Point(v, t, n);
						faces.add(p);
						sf.close();
					} else {
						//do nothing
					}
				}
				if (sc != null) sc.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanLine != null) scanLine.close();
		}
		
		//Load data into VBO
		int bufferSize = faces.size()*3;
		normalData = DoubleBuffer.allocate(bufferSize*3);
		textureData = DoubleBuffer.allocate(bufferSize*3);
		vertexData = DoubleBuffer.allocate(bufferSize*3);
		
		for (int i = 0; i < faces.size(); i++) {
			Point n = vertices.get((int)faces.get(i).z-1);
			normalData.put(n.x);
			normalData.put(n.z);
			normalData.put(n.y);
			
			Point p = vertices.get((int)faces.get(i).x-1);
			textureData.put(p.x);
			textureData.put(p.y);
			textureData.put(p.z);
			
			vertexData.put(p.x);
			vertexData.put(p.z);
			vertexData.put(p.y);
		}
		
		normalData.rewind();
		textureData.rewind();
		vertexData.rewind();
		
		gl.glGenBuffers(1, bufferIds, 0);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, bufferIds[0]);
		
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, bufferSize*3*Double.BYTES, null, GL2.GL_STATIC_DRAW);
		gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, bufferSize*Double.BYTES, normalData);
		gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, bufferSize*Double.BYTES, bufferSize*Double.BYTES, textureData);
		gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, bufferSize*2*Double.BYTES, bufferSize*Double.BYTES, vertexData);
		
		//Set shader program
		try {
   		 	shaderProgram = Shader.initShaders(gl,VERTEX_SHADER,FRAGMENT_SHADER);   		 
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
		
		texUnitLoc = gl.glGetUniformLocation(shaderProgram, "texUnit");
	}
}
